const cookieParser = require("cookie-parser");
const cors = require('cors');
const express = require("express");
const logger = require("morgan");
const grpc = require("@grpc/grpc-js");

// express routes
const apiRouter = require("./routes/express");
// grpc routes
const teamIAGService = require("./routes/grpc");
const teamService = require("./generated/teamIAG_grpc_pb.js")

// start express server
const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors())
app.use("/api", apiRouter);

// start grpc server
const grpcServer = new grpc.Server();
const PORT = 5000;

grpcServer.addService(teamService.TeamIAGService, teamIAGService);

grpcServer.bindAsync(`0.0.0.0:${PORT}`, grpc.ServerCredentials.createInsecure({
    'grpc.default_compression_level': 3, // (1->Low -- 3->High)
    'grpc.max_recieve_message_length': -1
}), (err, port) => {
    if (err) {
        console.error(`Error Starting GRPC Server: ${err}`);
    } else {
        console.log(`GRPC Server Running at http://0.0.0.0:${port}`);
        grpcServer.start();
    }
});

module.exports = app;
