const fs = require("fs");
const path = require("path");
const util = require('util');

const readFileAsync = util.promisify(fs.readFile);

const readPronghorn = async () => {
    const filePath = path.join(__dirname, '..', 'pronghorn.json');
    try {
        return await readFileAsync(filePath, 'utf8')
    } catch (err) {
        return err;
    }
}
const scriptsRes = {
    "data": [
        {
            "effective_schema_type": "user_schema",
            "file": "/usr/share/automation-gateway/scripts/sample_script.sh",
            "name": "sample_script.sh"
        },
        {
            "effective_schema_type": "user_schema",
            "file": "/usr/share/automation-gateway/scripts/sample_module.py",
            "name": "test_module.py"
        }
    ],
    "meta": {
        "count": 2,
        "queryObject": {
            "detail": "summary",
            "filter": {},
            "order": "ascending"
        }
    }
}

module.exports = { readPronghorn, scriptsRes }