const express = require("express");

const { readPronghorn, scriptsRes } = require("./utils")

const router = express.Router();

router.get('/', (req, res) => {
    res.send('Hello from non-gRPC server!');
});

router.get("/pronghorn", async (req, res, next) => {
    const pronghorn = await readPronghorn();
    res.send(pronghorn);
});

router.get("/scripts", async (req, res, next) => {
    res.send(scriptsRes)
})

module.exports = router;
