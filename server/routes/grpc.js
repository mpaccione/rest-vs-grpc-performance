const { readPronghorn, scriptsRes } = require("./utils")
const teamProto = require("../generated/teamIAG_pb.js")

const res = new teamProto.MessageResponse();

const teamIAGService = {
  sendMessage: async (call, callback) => {
    const pronghorn = await readPronghorn();
    res.setReply(pronghorn);
    callback(null, res);
  },
  streamMessage: async (call, callback) => {
    const pronghorn = await readPronghorn();
    res.setReply(pronghorn);
    call.write(res);
    call.end()
  },
  sendScripts: async (call, callback) => {
    res.setReply(scriptsRes)
    callback(null, res);
  },
  streamScripts: async (call, callback) => {
    const scriptResponse = new teamProto.ScriptResponse();

    scriptResponse.data = scriptsRes.data.map((script) => {
      const scriptInfo = new teamProto.ScriptInfo();
      scriptInfo.effective_schema_type = script.effective_schema_type;
      scriptInfo.file = script.file;
      scriptInfo.name = script.name;
      return scriptInfo;
    });

    scriptResponse.meta = new teamProto.Metadata();
    scriptResponse.meta.count = scriptsRes.meta.count;
    scriptResponse.meta.queryObject = scriptsRes.meta.queryObject;

    call.write(scriptResponse);
    call.end()
  },
}

module.exports = teamIAGService