// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var teamIAG_pb = require('./teamIAG_pb.js');

function serialize_EmptyRequest(arg) {
  if (!(arg instanceof teamIAG_pb.EmptyRequest)) {
    throw new Error('Expected argument of type EmptyRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_EmptyRequest(buffer_arg) {
  return teamIAG_pb.EmptyRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_MessageRequest(arg) {
  if (!(arg instanceof teamIAG_pb.MessageRequest)) {
    throw new Error('Expected argument of type MessageRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_MessageRequest(buffer_arg) {
  return teamIAG_pb.MessageRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_MessageResponse(arg) {
  if (!(arg instanceof teamIAG_pb.MessageResponse)) {
    throw new Error('Expected argument of type MessageResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_MessageResponse(buffer_arg) {
  return teamIAG_pb.MessageResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ScriptResponse(arg) {
  if (!(arg instanceof teamIAG_pb.ScriptResponse)) {
    throw new Error('Expected argument of type ScriptResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ScriptResponse(buffer_arg) {
  return teamIAG_pb.ScriptResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var TeamIAGService = exports.TeamIAGService = {
  sendMessage: {
    path: '/TeamIAG/SendMessage',
    requestStream: false,
    responseStream: false,
    requestType: teamIAG_pb.MessageRequest,
    responseType: teamIAG_pb.MessageResponse,
    requestSerialize: serialize_MessageRequest,
    requestDeserialize: deserialize_MessageRequest,
    responseSerialize: serialize_MessageResponse,
    responseDeserialize: deserialize_MessageResponse,
  },
  streamMessage: {
    path: '/TeamIAG/StreamMessage',
    requestStream: false,
    responseStream: true,
    requestType: teamIAG_pb.MessageRequest,
    responseType: teamIAG_pb.MessageResponse,
    requestSerialize: serialize_MessageRequest,
    requestDeserialize: deserialize_MessageRequest,
    responseSerialize: serialize_MessageResponse,
    responseDeserialize: deserialize_MessageResponse,
  },
  sendScripts: {
    path: '/TeamIAG/SendScripts',
    requestStream: false,
    responseStream: false,
    requestType: teamIAG_pb.EmptyRequest,
    responseType: teamIAG_pb.ScriptResponse,
    requestSerialize: serialize_EmptyRequest,
    requestDeserialize: deserialize_EmptyRequest,
    responseSerialize: serialize_ScriptResponse,
    responseDeserialize: deserialize_ScriptResponse,
  },
  streamScripts: {
    path: '/TeamIAG/StreamScripts',
    requestStream: false,
    responseStream: true,
    requestType: teamIAG_pb.EmptyRequest,
    responseType: teamIAG_pb.ScriptResponse,
    requestSerialize: serialize_EmptyRequest,
    requestDeserialize: deserialize_EmptyRequest,
    responseSerialize: serialize_ScriptResponse,
    responseDeserialize: deserialize_ScriptResponse,
  },
};

exports.TeamIAGClient = grpc.makeGenericClientConstructor(TeamIAGService);
