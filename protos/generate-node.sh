#!/bin/bash

PROTO_DIR="$(pwd)"
ROOT_DIR=$(dirname "$(pwd)")

/opt/homebrew/bin/grpc_tools_node_protoc \
  --plugin=protoc-gen-grpc=/opt/homebrew/bin/grpc_tools_node_protoc_plugin \
  --js_out=import_style=commonjs,binary:"${ROOT_DIR}/server/generated" \
  --grpc_out="${ROOT_DIR}/server/generated" \
  --proto_path="${PROTO_DIR}" \
  "${PROTO_DIR}/teamIAG.proto"


# Replace "require('grpc')" with "require('@grpc/grpc-js')"
find "${ROOT_DIR}/server/generated" -name '*.js' -exec sed -i '' 's%require('"'"'grpc'"'"')%require('"'"'@grpc/grpc-js'"'"')%g' {} +