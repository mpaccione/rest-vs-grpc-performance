#!/bin/bash

PROTO_DIR="$(pwd)"
ROOT_DIR=$(dirname "$(pwd)")

/opt/homebrew/bin/grpc_tools_node_protoc \
  --js_out=import_style=commonjs:"${ROOT_DIR}/client/src/generated" \
  --grpc-web_out=import_style=commonjs,mode=grpcwebtext:"${ROOT_DIR}/client/src/generated" \
  --proto_path="${PROTO_DIR}" \
  "${PROTO_DIR}/teamIAG.proto"
