import React, { useEffect, useState } from "react";
import axios from 'axios';

import "./App.css";
import { EmptyRequest, MessageRequest } from "./generated/teamIAG_pb"
import proto from './generated/teamIAG_grpc_web_pb';

const apiTimes = { pronghorn: '', scripts: '' }
const grpcClient = new proto.TeamIAGClient('http://localhost:8080');
const routes = [{ method: 'get', endpoint: 'pronghorn' }, { method: 'get', endpoint: 'scripts' }]

function App() {
  const [grpcProxy, setGRPCProxy] = useState(apiTimes)
  const [grpcProxyStream, setGRPCProxyStream] = useState(apiTimes)
  const [rest, setRest] = useState(apiTimes)
  const [restProxy, setRestProxy] = useState(apiTimes)

  const initRestReqs = async () => {
    for (const route of routes) {
      const startRest = performance.now();

      if (route.method === 'get') {
        await axios.get(`http://localhost:4000/api/${route.endpoint}`)
        const endRest = performance.now() - startRest;
        await setRest((prevRest) => ({ ...prevRest, [route.endpoint]: endRest }))
      }
    }
  }

  const initRestProxyReqs = async () => {
    for (const route of routes) {
      const startRestProxy = performance.now();

      if (route.method === 'get') {
        await axios.get(`http://localhost:8081/api/${route.endpoint}`)
        const endRestProxy = performance.now() - startRestProxy;
        await setRestProxy((prevRestProxy) => ({ ...prevRestProxy, [route.endpoint]: endRestProxy }))
      }
    }
  }

  const initUnaryGRPC = async () => {
    const map = { pronghorn: 'sendMessage', scripts: 'sendScripts' }
    for (const route of routes) {
      const startGRPC = performance.now();
      const request = new MessageRequest()

      // request.setText('Test')

      grpcClient[map[route.endpoint]](request, null, (err, res) => {
        const endGRPC = performance.now() - startGRPC;
        console.log({ res })
        setGRPCProxy((prevGRPCProxy) => ({ ...prevGRPCProxy, [route.endpoint]: endGRPC }))
      });
    }
  }

  const initStreamGRPC = async () => {
    const map = { pronghorn: 'streamMessage', scripts: 'streamScripts' }
    for (const route of routes) {
      const startGRPC = performance.now();
      const request = route.endpoint === 'scripts' ? new EmptyRequest() : new MessageRequest()

      // request.setText('Test')

      const stream = grpcClient[map[route.endpoint]](request);

      stream.on('end', () => {
        const endGRPCStream = performance.now() - startGRPC;
        setGRPCProxyStream((prevGRPCProxyStream) => ({ ...prevGRPCProxyStream, [route.endpoint]: endGRPCStream }));
      });

      stream.on('error', (err) => console.error(err))
    }
  }

  const init = async () => {
    await initRestReqs()
    await initRestProxyReqs()
    await initUnaryGRPC()
    await initStreamGRPC();
  }

  useEffect(() => {
    // delay to allow for initial rendering
    setTimeout(() => {
      init()
    }, 500)
  }, [])

  return (
    <div className="App">
      <table style={{ margin: "auto", textAlign: "right", width: "80%" }}>
        <thead>
          <tr>
            <th>Endpoint</th>
            <th></th>
            <th>REST</th>
            <th>REST Proxy</th>
            <th>Unary GRPC</th>
            <th>Stream GRPC</th>
          </tr>
        </thead>
        <tbody>
          {routes.map(({ endpoint, method }, idx) => (
            <tr key={idx}>
              <td>{method.toUpperCase()}</td>
              <td style={{ textAlign: "left" }}>/{endpoint}</td>
              <td>{parseInt(rest[endpoint])} ms</td>
              <td>{parseInt(restProxy[endpoint])} ms</td>
              <td>{parseInt(grpcProxy[endpoint])} ms</td>
              <td>{parseInt(grpcProxyStream[endpoint])} ms</td>
            </tr>
          )
          )}
        </tbody>
      </table>
    </div>
  )
};

export default App;
